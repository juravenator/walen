#!/usr/bin/env python3
import json
import os
import sys
import logging

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "./vendored"))


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

import requests
from PIL import Image, ImageFont, ImageDraw

TOKEN = os.environ['TELEGRAM_TOKEN']
BASE_URL = "https://api.telegram.org/bot{}".format(TOKEN)


def walen(event, context):
    im = Image.open("walen.png").convert("RGB")
    d = ImageDraw.Draw(im)
    try:
        data = json.loads(event["body"])
        message = str(data["message"]["text"])
        chat = data["message"]["chat"]
        chat_id = chat["id"]
        logging.warning('received request "%s" from user "%s" (name: %s) (id: %s)', message, chat["username"], chat["first_name"], chat["id"])

        fontsize = 10
        while fontsize < 100:
            size = d.textsize(message, font=ImageFont.truetype("./impact.ttf", fontsize + 1))
            if size[0] > 114:
                break
            else:
                fontsize += 1
        
        fnt = ImageFont.truetype("./impact.ttf", fontsize)
        size = d.textsize(message, font=fnt)
        if size[0] > 114:
            logging.info("message too big")
            requests.post(BASE_URL + "/sendMessage", {"text": "sorry, that text is too big to fit", "chat_id": chat_id})
            return {"statusCode": 200}
        y = 100
        if fontsize > 40:
            y -= fontsize / 2
        d.text((365, y), message, font=fnt, fill=(0,0,0))

        try:
            im.save("/tmp/walen.png")
            status = requests.post(BASE_URL + "/sendPhoto?chat_id=" + str(chat_id), files={"photo": open("/tmp/walen.png", "rb")})
            status.raise_for_status()
        except Exception as err:
            logger.exception("could not send image")
            requests.post(BASE_URL + "/sendMessage", {"text": "unable to send image: " + repr(err), "chat_id": chat_id})

    except Exception:
        logger.exception("unhandled error")

    return {"statusCode": 200}
